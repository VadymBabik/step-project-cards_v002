# Step Project "CARDS"

**email**: step@project.cards<br/>
**password**: step@project.cards<br/>
**token**: 7b4afba8-3267-406f-8280-a610a6985812<br/>

**Участники:**

- Adel Al-Makhmud<br/>
- VadymBabik<br/>
- Maksim.L<br/>

**Adel Al-Makhmud**<br/>
Классы компонентов (components folder), Класс Form, (form Dentist, form Cardiologist, form Therapist) Класс Visit и его под классы (без удаления и редактирования). CardAPI, и info.js.

**VadymBabik**<br/>
Parcel сборка, Entry (функционал входа), Filter (функционал фильтрации и поиска), Layout (SCSS, Bootstrap, HTML)

**Maksim.L**<br/>
Переключение выбора докторов(SelectDoctor, FormSelect), class Modal (и подклассы), редактирование и удаление визитов (методы в Visit.js), рендеринг (renderCards.js) и стилизация карточек, Bootstrap